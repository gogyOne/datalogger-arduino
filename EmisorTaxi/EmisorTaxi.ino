#include <SdFat.h>
#include <SDLib.h>
#include <SoftwareSerial.h>
#include <TinyGPS.h>
#include "DHT.h"
#include <SensorData.h>
#include <LibXBee.h>
#include <XBee.h>

//set baudios sh sl receptor
LibXBee xbee3(115200, 0x0013A200, 0x40A1EBC4);

//set pin de dht22, pin del led, pin de sensor dust
SensorData sensor(5, 4, A3);
//set pin SD
SDLib sd1;
int idUsuario = 2;

unsigned long tiempoMuestreo1 = 700;
unsigned long last = 0;

SdFat sd;

void setup() {
  Serial.begin(115200);
  while (!Serial) {}
  //inicia la memoria SD
  if (!sd.begin(8, SPI_FULL_SPEED)) sd.initErrorHalt();
  Serial.println("iniciado");
}
void loop() {
  //ciclo de escritura de datos en la SD
  unsigned long start = millis();
  if (start - last >= tiempoMuestreo1) {
    last = start;
    //prepara valores a almacenar
    String total = "0,";
    total.concat(sensor.getTemperaturaC());
    total.concat(",");
    total.concat(sensor.getHumedad());
    total.concat(",");
    total.concat(sensor.getLocation());
    total.concat(",");
    total.concat(sensor.getMP());
    total.concat(",");
    total.concat(idUsuario);
    total.concat(",");
    //convierte a array de char para guardar
    char aux[total.length() + 1];
    total.toCharArray(aux, total.length() + 1);
    //Almacena valor en la SD que no esta repetido
    sd1.guardarData(aux);
  }



  //lee una trama de la tarjeta sd que aun no se ha enviado
  String trama = sd1.getData0();
  //ejecuta esta condicion solo si existen tramas no enviadas (trama que tiene un cero al principio)
  if (trama != "\0") {
    //si al enviar la trama, este la recibe
    //ciclo de envio de datos al servidor
    if (xbee3.enviar(trama) == true) {
      Serial.println("");
      Serial.print("La trama ");
      Serial.print(trama);
      Serial.print(" a sido enviada, recibida");
      //convierte la trama existente que tiene cero, a arreglo de char
      int largo = trama.length() + 1;
      char a[largo];
      trama.toCharArray(a, largo);
      //modifica con un uno al principio de la trama existente
      boolean h = sd1.rewrite(a);
      if (h) {
        Serial.println(" y reescrita");
      } else {
        Serial.println(" y no reescrita");
      }

    } else {
      Serial.println("trama perdida");
    }
  } else {
    Serial.println("trama nula");
  }
}

