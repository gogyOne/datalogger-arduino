#include <LibXBeeEnd.h>
#include <XBee.h>

LibXBeeEnd xbee3(115200);

void setup() {
  Serial.begin(115200);
}

void loop() {
  String cadena=xbee3.recibirPaquete();
  if(cadena!="\0"){
    Serial.print(cadena);
    Serial.flush();
  }
}
