package vista2;

import domain.DataSensor;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;

// TODO: Auto-generated Javadoc
/**
 * The Class SerialTest.
 */
public class SerialTest implements SerialPortEventListener {

    /** The serial port. */
    SerialPort serialPort;
    /**
     * The port we're normally going to use.
     */
    private static final String PORT_NAMES[] = {
        //"/dev/tty.usbserial-A9007UX1", // Mac OS X
        //"/dev/ttyUSB0", // Linux
        "COM7",// Windows
    };
    
    /** A BufferedReader which will be fed by a InputStreamReader converting the bytes into characters making the displayed results codepage independent. */
    private DataInputStream input;
    
    /** The output stream to the port. */
    private OutputStream output;
    
    /** Milliseconds to block while waiting for port open. */
    private static final int TIME_OUT = 2000;
    /**
     * Default bits per second for COM port.
     */
    private static final int DATA_RATE = 115200;

    /**
     * Initialize.
     */
    public void initialize() {
        CommPortIdentifier portId = null;
        Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();

        //First, Find an instance of serial port as set in PORT_NAMES.
        while (portEnum.hasMoreElements()) {
            CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
            for (String portName : PORT_NAMES) {
                if (currPortId.getName().equals(portName)) {
                    portId = currPortId;
                    break;
                }
            }
        }
        if (portId == null) {
            System.out.println("Could not find COM port.");
            return;
        }

        try {
            // open serial port, and use class name for the appName.
            serialPort = (SerialPort) portId.open(this.getClass().getName(),
                    TIME_OUT);

            // set port parameters
            serialPort.setSerialPortParams(DATA_RATE,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);

            // open the streams
            input = new DataInputStream(serialPort.getInputStream());
            output = serialPort.getOutputStream();

            // add event listeners
            serialPort.addEventListener(this);
            serialPort.notifyOnDataAvailable(true);
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }

    /**
     * This should be called when you stop using the port. This will prevent
     * port locking on platforms like Linux.
     */
    public synchronized void close() {
        if (serialPort != null) {
            serialPort.removeEventListener();
            serialPort.close();
        }
    }

    /**
     * Handle an event on the serial port. Read the data and print it.
     *
     * @param oEvent the o event
     */
    public synchronized void serialEvent(SerialPortEvent oEvent) {
        if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
            try {
                //lee la linea que imprime Serial.print(), la funcion que imprime en Arduino
                String inputLine = input.readLine();
                //imprime la linea
                System.out.println(inputLine);
                //crea objeto StringTokenizer
                StringTokenizer token = new StringTokenizer(inputLine);
                //crea arreglo dinamico
                Vector<String> vector = new Vector<String>();
                /*agrega valores al arreglo a partir de la linea 
                con valores separados por coma*/
                while (token.hasMoreTokens()) {
                    vector.add(token.nextToken(","));
                }
                /*almacena la temperatura, humedad, latitud, longitud,
                  material particula e identificador de usuario */
                float temp = Float.parseFloat(vector.get(1));
                float hum = Float.parseFloat(vector.get(2));
                double lat = Double.parseDouble(vector.get(3));
                double lon = Double.parseDouble(vector.get(4));
                float mp = Float.parseFloat(vector.get(5));
                int idUsuario1 = Integer.parseInt(vector.get(6));
                webservice.ServicioTodo servicio = new webservice.ServicioTodo();
                double vacio = (double) 0.0;
                //si los datos de sensores son distintos de nulo
                if (lat != vacio && lon != vacio && temp != vacio && hum != vacio && mp != vacio && idUsuario1 != 0) {
                    //crea objeto DataSensor
                    DataSensor data = new DataSensor(temp, hum, lat, lon, mp, idUsuario1);
                    /*si los valores obtenidos desde los sensores son unicos,
                     entonces agrega este valor a la base de datos */
                    if (servicio.estaRepetido(data) == false) {
                        servicio.addDataSensor(data);
                    } else {
                        System.out.println("Datos de sensores ya existen");
                    }

                } else {
                    System.out.println("Error. Datos del sensor no deben ser vacios");
                }

            } catch (Exception e) {
                System.err.println(e.toString());
            }
        }
        // Ignore all the other eventTypes, but you should consider the other ones.
    }

    /**
     * The main method.
     *
     * @param args the arguments
     * @throws Exception the exception
     */
    public static void main(String[] args) throws Exception {
        SerialTest main = new SerialTest();
        main.initialize();
        Thread t = new Thread() {
            public void run() {
                //the following line will keep this app alive for 1000 seconds,
                //waiting for events to occur and responding to them (printing incoming messages to console).
                try {
                    Thread.sleep(1000000);
                } catch (InterruptedException ie) {
                }
            }
        };
        t.start();
        System.out.println("Started");
    }
}
