package domain;

// TODO: Auto-generated Javadoc
/**
 * The Class DataSensor.
 */
public class DataSensor {

    /** The temp. */
    private float temp;
    
    /** The hum. */
    private float hum;
    
    /** The lat. */
    private double lat;
    
    /** The lon. */
    private double lon;
    
    /** The mp. */
    private float mp;
    
    /** The id usuario. */
    private int idUsuario;

    /**
     * Gets the temp.
     *
     * @return the temp
     */
    public float getTemp() {
        return temp;
    }

    /**
     * Sets the temp.
     *
     * @param temp the new temp
     */
    public void setTemp(float temp) {
        this.temp = temp;
    }

    /**
     * Gets the hum.
     *
     * @return the hum
     */
    public float getHum() {
        return hum;
    }

    /**
     * Sets the hum.
     *
     * @param hum the new hum
     */
    public void setHum(float hum) {
        this.hum = hum;
    }

    /**
     * Gets the lat.
     *
     * @return the lat
     */
    public double getLat() {
        return lat;
    }

    /**
     * Sets the lat.
     *
     * @param lat the new lat
     */
    public void setLat(double lat) {
        this.lat = lat;
    }

    /**
     * Gets the lon.
     *
     * @return the lon
     */
    public double getLon() {
        return lon;
    }

    /**
     * Sets the lon.
     *
     * @param lon the new lon
     */
    public void setLon(double lon) {
        this.lon = lon;
    }

    /**
     * Gets the mp.
     *
     * @return the mp
     */
    public float getMp() {
        return mp;
    }

    /**
     * Sets the mp.
     *
     * @param mp the new mp
     */
    public void setMp(float mp) {
        this.mp = mp;
    }

    /**
     * Gets the id usuario.
     *
     * @return the id usuario
     */
    public int getIdUsuario() {
        return idUsuario;
    }

    /**
     * Sets the id usuario.
     *
     * @param idUsuario the new id usuario
     */
    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * Instantiates a new data sensor.
     */
    public DataSensor() {
    }

    /**
     * Instantiates a new data sensor.
     *
     * @param temp the temp
     * @param hum the hum
     * @param lat the lat
     * @param lon the lon
     * @param mp the mp
     * @param idUsuario the id usuario
     */
    public DataSensor(float temp, float hum, double lat, double lon, float mp, int idUsuario) {
        this.temp = temp;
        this.hum = hum;
        this.lat = lat;
        this.lon = lon;
        this.mp = mp;
        this.idUsuario = idUsuario;
    }
    
}
