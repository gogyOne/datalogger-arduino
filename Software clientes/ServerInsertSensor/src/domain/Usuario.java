/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package domain;

// TODO: Auto-generated Javadoc
/**
 * The Class Usuario.
 *
 * @author Usuario
 */
public class Usuario {
    
    /** The id usuario. */
    private int idUsuario;
    
    /** The username. */
    private String username;
    
    /** The password. */
    private String password;
    
    /** The tipo usuario. */
    private String tipoUsuario;

    /**
     * Instantiates a new usuario.
     */
    public Usuario() {
    }

    /**
     * Gets the id usuario.
     *
     * @return the id usuario
     */
    public int getIdUsuario() {
        return idUsuario;
    }

    /**
     * Sets the id usuario.
     *
     * @param idUsuario the new id usuario
     */
    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
    
    /**
     * Gets the username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username.
     *
     * @param username the new username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets the password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password.
     *
     * @param password the new password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets the tipo usuario.
     *
     * @return the tipo usuario
     */
    public String getTipoUsuario() {
        return tipoUsuario;
    }

    /**
     * Sets the tipo usuario.
     *
     * @param tipoUsuario the new tipo usuario
     */
    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    /**
     * Instantiates a new usuario.
     *
     * @param username the username
     * @param password the password
     * @param tipoUsuario the tipo usuario
     */
    public Usuario(String username, String password, String tipoUsuario) {
        this.username = username;
        this.password = password;
        this.tipoUsuario = tipoUsuario;
    }
    
    
    
}
