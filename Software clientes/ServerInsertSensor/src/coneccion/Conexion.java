/**
 * 
 */
package coneccion;

import java.sql.Connection;
import java.sql.DriverManager;

// TODO: Auto-generated Javadoc
/**
 * The Class Conexion.
 *
 * @author Usuario
 */
public class Conexion {
	
	/** The user. */
	private String user="root";
	
	/** The pass. */
	private String pass="";
	
	/** The name bd. */
	private String nameBD="bdtaxi";
	
	/** The connection. */
	private Connection connection;
	
	/**
	 * Instantiates a new conexion.
	 */
	public Conexion(){
		
	}
	
	/**
	 * Conectar.
	 *
	 * @return the conexion
	 */
	public Conexion conectar(){
		Connection conn;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			conn=DriverManager.getConnection("jdbc:mysql://localhost/"+nameBD, this.user,this.pass);
			this.setConnection(conn);
			if(this.getConnection()==null){
				//System.out.println("Conexion fallida");
			}else{
				//System.out.println("Conexion exitosa");
			}
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		return this;
	}

	/**
	 * Gets the connection.
	 *
	 * @return the connection
	 */
	public Connection getConnection() {
		return connection;
	}

	/**
	 * Sets the connection.
	 *
	 * @param connection the connection to set
	 */
	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
}
