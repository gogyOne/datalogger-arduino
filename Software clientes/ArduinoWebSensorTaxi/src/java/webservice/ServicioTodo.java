package webservice;

import java.sql.ResultSet;
import java.sql.SQLException;

import coneccion.Conexion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Statement;

import coneccion.Conexion;

import domain.DataSensor;
import domain.Usuario;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// TODO: Auto-generated Javadoc
/**
 * The Class ServicioTodo.
 */
public class ServicioTodo {

    /** The con1. */
    private Conexion con1;

    /**
     * Instantiates a new servicio todo.
     */
    public ServicioTodo() {

    }

    /**
     * Checks if is email.
     *
     * @param correo the correo
     * @return true, if is email
     */
    public boolean isEmail(String correo) {
        Pattern pat = null;
        Matcher mat = null;
        pat = Pattern
                .compile("^([0-9a-zA-Z]([_.w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-w]*[0-9a-zA-Z].)+([a-zA-Z]{2,9}.)+[a-zA-Z]{2,3})$");
        mat = pat.matcher(correo);
        if (mat.find()) {
            System.out.println("[" + mat.group() + "]");
            return true;
        } else {
            return false;
        }
    }

    /**
     * Validar cuenta.
     *
     * @param user the user
     * @param pass the pass
     * @return true, if successful
     */
    public boolean validarCuenta(String user, String pass) {
        boolean res = false;
        if (getUsuarioByUserPass(user, pass) == null) {
            res = false;
        } else {
            res = true;
        }
        return res;
    }

    /**
     * Removes the usuario by user.
     *
     * @param user the user
     * @return the string
     */
    public String removeUsuarioByUser(String user) {
        String out = "";
        int res = this.crearUpdate("delete from usuario where username='" + user + "'");
        if (res == 1) {
            out = "Borrado de usuario " + user + " exitoso";
        } else {
            out = "Error al borrar usuario " + user;
        }
        return out;
    }

    /**
     * Gets the usuario by user pass.
     *
     * @param user the user
     * @param pass the pass
     * @return the usuario by user pass
     */
    public List<Usuario> getUsuarioByUserPass(String user, String pass) {
        ResultSet resultado = crearConsulta("select * from usuario where username='" + user + "' and password='" + pass + "'");
        List<Usuario> lista = new ArrayList<Usuario>();
        try {
            while (resultado.next()) {
                Usuario usuarioVO = new Usuario();

                usuarioVO.setUsername((String) resultado.getObject("username"));
                usuarioVO.setPassword((String) resultado.getObject("password"));
                usuarioVO.setTipoUsuario((String) resultado.getObject("tipoUsuario"));
                lista.add(usuarioVO);
            }
        } catch (Exception e) {
            e.printStackTrace();
            lista = null;
        }
        try {
            this.getCon1().getConnection().close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return lista;
    }

    /**
     * Gets the tipo usuario.
     *
     * @param user the user
     * @param pass the pass
     * @return the tipo usuario
     */
    public String getTipoUsuario(String user, String pass) {
        String a = "";
        try {
            Usuario usuario = this.getUsuarioByUserPass(user, pass).get(0);
            a = usuario.getTipoUsuario();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return a;
    }

    /**
     * Adds the usuario.
     *
     * @param usuario the usuario
     * @return the string
     */
    public String addUsuario(Usuario usuario) {
        String resp = "";
        // valida campos vacios

        if (usuario.getPassword() != null) {
            if (usuario.getTipoUsuario() != null) {
                if (usuario.getUsername() != null && this.isEmail(usuario.getUsername()) == true) {
                    String sql = "insert into usuario (username, password, tipoUsuario) value ('"
                            + usuario.getUsername()
                            + "','"
                            + usuario.getPassword()
                            + "','"
                            + usuario.getTipoUsuario()
                            + "')";
                    if (crearUpdate(sql) > 0) {
                        resp = "Datos de usuario agregados con exito";
                    } else {
                        resp = "Fallo al agregar datos de usuario";
                    }
                } else {
                    resp = "campo usuario no debe ser vacio y debe ser valido";
                }

            } else {
                resp = "campo 'tipoUsuario' no debe ser vacio";
            }
        } else {
            resp = "campo password no debe ser vacio";
        }

        try {
            this.getCon1().getConnection().close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return resp;
    }

    /**
     * Adds the data sensor.
     *
     * @param al the al
     * @return the string
     */
    public String addDataSensor(DataSensor al) {
        String resp = "";

        // valida campos vacios
        if (al.getTemp() != 0.0) {
            if (al.getHum() != 0.0) {
                if (al.getLat() != 0.0) {
                    if (al.getLon() != 0.0) {
                        if (al.getMp() != 0.0) {
                            String sql = "insert into datasensor (temp, hum, lat, lon, UsuarioidUsuario, mp) value ("
                                    + al.getTemp()
                                    + ","
                                    + al.getHum()
                                    + ","
                                    + al.getLat()
                                    + ","
                                    + al.getLon()
                                    + ","
                                    + al.getIdUsuario()
                                    + ","
                                    +al.getMp()
                                    + ")";
                            if (crearUpdate(sql) > 0) {
                                resp = "Datos de sensores agregados con exito";
                            } else {
                                resp = "Fallo al agregar datos de sensor";
                            }
                        } else {
                            resp = "campo MP no deber ser vacio";
                        }
                    } else {
                        resp = "campo lon no debe ser vacio";
                    }
                } else {
                    resp = "campo lat no debe ser vacio";
                }
            } else {
                resp = "campo hum no debe ser vacio";
            }
        } else {
            resp = "campo temp no debe ser vacio";
        }
        try {
            this.getCon1().getConnection().close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return resp;
    }

    /**
     * Gets the todo usuario.
     *
     * @return the todo usuario
     */
    public List<Usuario> getTodoUsuario() {
        ResultSet resultado = crearConsulta("select * from usuario");
        List<Usuario> lista = new ArrayList<Usuario>();
        try {
            while (resultado.next()) {
                Usuario usuario = new Usuario();
                usuario.setIdUsuario(Integer.parseInt("" + resultado.getObject("idUsuario")));
                usuario.setPassword((String) resultado.getObject("password"));
                usuario.setTipoUsuario((String) resultado.getObject("tipoUsuario"));
                usuario.setUsername((String) resultado.getObject("username"));
                lista.add(usuario);
            }
        } catch (Exception e) {
            e.printStackTrace();
            lista = null;
        }
        try {
            this.getCon1().getConnection().close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return lista;
    }

    /**
     * Ver data by usuario.
     *
     * @param user the user
     * @param pass the pass
     * @return the list
     */
    public List<DataSensor> verDataByUsuario(String user, String pass) {
        ResultSet resultado = crearConsulta("select temp, hum, lat, lon, UsuarioidUsuario, mp from datasensor inner join usuario where usuario.username='" + user + "' and usuario.password='" + pass + "'");
        List<DataSensor> lista = new ArrayList<DataSensor>();
        try {
            while (resultado.next()) {
                DataSensor al = new DataSensor();
                al.setTemp(Float.parseFloat(""+resultado.getObject(1)));
                al.setHum(Float.parseFloat(""+resultado.getObject(2)));
                al.setLat(Float.parseFloat(""+resultado.getObject(3)));
                al.setLon(Float.parseFloat(""+resultado.getObject(4)));
                al.setIdUsuario(Integer.parseInt(resultado.getObject(5) + ""));
                al.setMp(Float.parseFloat(""+resultado.getObject(6)));
                lista.add(al);
            }
        } catch (Exception e) {
            e.printStackTrace();
            lista = null;
        }
        try {
            if (resultado.next() == false) {
                //lista = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            this.getCon1().getConnection().close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return lista;
    }
    
    /**
     * Esta repetido.
     *
     * @param data the data
     * @return true, if successful
     */
    public boolean estaRepetido(DataSensor data){
        List<DataSensor> lista=getTodo();
        boolean res=false;
        for(int cont=0;cont<lista.size();cont++){
            DataSensor antiguo=lista.get(cont);
            if(data.getHum()==antiguo.getHum()
                    && data.getIdUsuario()==antiguo.getIdUsuario()
                    && data.getLat()==antiguo.getLat()
                    && data.getLon()==antiguo.getLon()
                    && data.getMp()==antiguo.getMp()
                    && data.getTemp()==antiguo.getTemp()){
                res=true;
                break;
            }
        }
        return res;
    }
    
    /**
     * Gets the todo.
     *
     * @return the todo
     */
    public List<DataSensor> getTodo() {
        ResultSet resultado = crearConsulta("select temp, hum, lat, lon, UsuarioidUsuario, mp from datasensor");
        List<DataSensor> lista = new ArrayList<DataSensor>();
        try {
            while (resultado.next()) {
                DataSensor al = new DataSensor();
                al.setTemp(Float.parseFloat(""+resultado.getObject(1)));
                al.setHum(Float.parseFloat(""+resultado.getObject(2)));
                al.setLat(Float.parseFloat(""+resultado.getObject(3)));
                al.setLon(Float.parseFloat(""+resultado.getObject(4)));
                al.setIdUsuario(Integer.parseInt(resultado.getObject(5) + ""));
                al.setMp(Float.parseFloat(""+resultado.getObject(6)));
                lista.add(al);
            }
        } catch (Exception e) {
            e.printStackTrace();
            lista = null;
        }
        try {
            this.getCon1().getConnection().close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return lista;
    }

    /**
     * Gets the con1.
     *
     * @return the con1
     */
    public Conexion getCon1() {
        return con1;
    }

    /**
     * Sets the con1.
     *
     * @param con1 the con1 to set
     */
    public void setCon1(Conexion con1) {
        this.con1 = con1;
    }

    /**
     * Crear update.
     *
     * @param sql the sql
     * @return the int
     */
    public int crearUpdate(String sql) {
        int resultado = 0;
        coneccion.Conexion con = new Conexion().conectar();
        this.setCon1(con);
        try {
            Statement sentencia = (Statement) con.getConnection()
                    .createStatement();
            resultado = sentencia.executeUpdate(sql);
            //sentencia.close();
            //con.getConnection().close();
        } catch (SQLException e) {
            /*
             try {
             con.getConnection().rollback();
             } catch (SQLException e1) {
             // TODO Auto-generated catch block
             e1.printStackTrace();
             }*/
        }
        return resultado;

    }

    /**
     * Crear consulta.
     *
     * @param sql the sql
     * @return the result set
     */
    public ResultSet crearConsulta(String sql) {
        ResultSet resultado = null;
        coneccion.Conexion con = new coneccion.Conexion();
        con.conectar();
        this.setCon1(con);
        try {

            Statement sentencia = (Statement) con.getConnection()
                    .createStatement();
            resultado = sentencia.executeQuery(sql);
            //sentencia.close();
            //con.getConnection().close();
        } catch (Exception e) {
            e.printStackTrace();
            /*try {
             con.getConnection().rollback();
             } catch (SQLException e1) {
             // TODO Auto-generated catch block
             e1.printStackTrace();
             }*/
        }

        return resultado;
    }
}
