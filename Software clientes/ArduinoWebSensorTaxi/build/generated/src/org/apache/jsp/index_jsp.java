package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import domain.DataSensor;
import webservice.ServicioTodo;
import java.util.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE html>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<html>\n");
      out.write("  <head>\n");
      out.write("    <meta charset=\"utf-8\">\n");
      out.write("    <title>Heatmaps</title>\n");
      out.write("    <style>\n");
      out.write("      html, body, #map-canvas {\n");
      out.write("        height: 100%;\n");
      out.write("        margin: 0px;\n");
      out.write("        padding: 0px\n");
      out.write("      }\n");
      out.write("      #panel {\n");
      out.write("        position: absolute;\n");
      out.write("        top: 5px;\n");
      out.write("        left: 50%;\n");
      out.write("        margin-left: -180px;\n");
      out.write("        z-index: 5;\n");
      out.write("        background-color: #fff;\n");
      out.write("        padding: 5px;\n");
      out.write("        border: 1px solid #999;\n");
      out.write("      }\n");
      out.write("    </style>\n");
      out.write("    <script src=\"https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=visualization\"></script>\n");
      out.write("    <script>\n");
      out.write("// Adding 500 Data Points\n");
      out.write("var map, pointarray, heatmap;\n");
      out.write("\n");
      out.write("var taxiData = [\n");
      out.write("  ");

  ServicioTodo servicio=new ServicioTodo();
  List<DataSensor> lista=new ArrayList<DataSensor>();
  lista=servicio.getTodo();
  String salida="";
  for(int cont=0;cont<lista.size();cont++){
	 String lat=lista.get(cont).getLat();
	 String lon=lista.get(cont).getLon();
	 if(!lat.equalsIgnoreCase("0.00000000000000")&&!lon.equalsIgnoreCase("0.00000000000000")){
  		salida+="new google.maps.LatLng("+lat+","+lon+"),\n";
	 } 
  }
  salida=salida.substring(0,salida.length()-1);
  
      out.write("\n");
      out.write("  ");
      out.print(salida);
      out.write("\n");
      out.write("  //new google.maps.LatLng(37.782551, -122.445368),\n");
      out.write("  //new google.maps.LatLng(37.751266, -122.403355)\n");
      out.write("];\n");
      out.write("\n");
      out.write("function initialize() {\n");
      out.write("  var mapOptions = {\n");
      out.write("    zoom: 13,\n");
      out.write("    ");
 
    String out2="";
    if(lista.size()>=1){
    	out2="center: new google.maps.LatLng("+lista.get(lista.size()-1).getLat()+", "+lista.get(lista.size()-1).getLon()+"),";
    }
    
      out.write("\n");
      out.write("    ");
      out.print(out2);
      out.write("\n");
      out.write("    //center: new google.maps.LatLng(37.774546, -122.433523),\n");
      out.write("    mapTypeId: google.maps.MapTypeId.SATELLITE\n");
      out.write("  };\n");
      out.write("\n");
      out.write("  map = new google.maps.Map(document.getElementById('map-canvas'),\n");
      out.write("      mapOptions);\n");
      out.write("\n");
      out.write("  var pointArray = new google.maps.MVCArray(taxiData);\n");
      out.write("  ");
int indice=0;
      out.write("\n");
      out.write("  for(var cont=0;cont<taxiData.length;cont++){\n");
      out.write("      var marker = new google.maps.Marker({\n");
      out.write("        position: taxiData[cont],\n");
      out.write("        map: map,\n");
      out.write("        title:\n");
      out.write("        ");

        String out3="";
        out3+="'"+lista.get(indice).getTemp()+" celcius, "+lista.get(indice).getHum()+"% de humedad'";
        indice++;
        
      out.write("\n");
      out.write("        ");
      out.print(out3);
      out.write("\n");
      out.write("    });\n");
      out.write("  }\n");
      out.write("  ");
indice=0;
      out.write("\n");
      out.write("  heatmap = new google.maps.visualization.HeatmapLayer({\n");
      out.write("    data: pointArray\n");
      out.write("  });\n");
      out.write("\n");
      out.write("  heatmap.setMap(map);\n");
      out.write("}\n");
      out.write("function toggleHeatmap() {\n");
      out.write("  heatmap.setMap(heatmap.getMap() ? null : map);\n");
      out.write("}\n");
      out.write("\n");
      out.write("function changeGradient() {\n");
      out.write("  var gradient = [\n");
      out.write("    'rgba(0, 255, 255, 0)',\n");
      out.write("    'rgba(0, 255, 255, 1)',\n");
      out.write("    'rgba(0, 191, 255, 1)',\n");
      out.write("    'rgba(0, 127, 255, 1)',\n");
      out.write("    'rgba(0, 63, 255, 1)',\n");
      out.write("    'rgba(0, 0, 255, 1)',\n");
      out.write("    'rgba(0, 0, 223, 1)',\n");
      out.write("    'rgba(0, 0, 191, 1)',\n");
      out.write("    'rgba(0, 0, 159, 1)',\n");
      out.write("    'rgba(0, 0, 127, 1)',\n");
      out.write("    'rgba(63, 0, 91, 1)',\n");
      out.write("    'rgba(127, 0, 63, 1)',\n");
      out.write("    'rgba(191, 0, 31, 1)',\n");
      out.write("    'rgba(255, 0, 0, 1)'\n");
      out.write("  ]\n");
      out.write("  heatmap.set('gradient', heatmap.get('gradient') ? null : gradient);\n");
      out.write("}\n");
      out.write("\n");
      out.write("function changeRadius() {\n");
      out.write("  heatmap.set('radius', heatmap.get('radius') ? null : 20);\n");
      out.write("}\n");
      out.write("\n");
      out.write("function changeOpacity() {\n");
      out.write("  heatmap.set('opacity', heatmap.get('opacity') ? null : 0.2);\n");
      out.write("}\n");
      out.write("\n");
      out.write("google.maps.event.addDomListener(window, 'load', initialize);\n");
      out.write("\n");
      out.write("    </script>\n");
      out.write("  </head>\n");
      out.write("\n");
      out.write("  <body>\n");
      out.write("    <div id=\"panel\">\n");
      out.write("      <button onclick=\"toggleHeatmap()\">Toggle Heatmap</button>\n");
      out.write("      <button onclick=\"changeGradient()\">Change gradient</button>\n");
      out.write("      <button onclick=\"changeRadius()\">Change radius</button>\n");
      out.write("      <button onclick=\"changeOpacity()\">Change opacity</button>\n");
      out.write("    </div>\n");
      out.write("    <div id=\"map-canvas\"></div>\n");
      out.write("  </body>\n");
      out.write("</html>\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
