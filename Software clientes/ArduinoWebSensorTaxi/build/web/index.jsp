<!DOCTYPE html>
<%@page import="domain.DataSensor"%>
<%@page import="webservice.ServicioTodo"%>
<%@page import="java.util.*"%>
<html>
    <head>
        <meta charset="utf-8">
        <title>Heatmaps</title>
        <style>
            html, body, #map-canvas {
                height: 100%;
                margin: 0px;
                padding: 0px
            }
            #panel {
                position: absolute;
                top: 5px;
                left: 50%;
                margin-left: -180px;
                z-index: 5;
                background-color: #fff;
                padding: 5px;
                border: 1px solid #999;
            }
        </style>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=visualization"></script>
        <script>
                    // Adding 500 Data Points
                    var map, pointarray, heatmap;
                    var taxiData = [
            <%
                ServicioTodo servicio = new ServicioTodo();
                List<DataSensor> lista = new ArrayList<DataSensor>();
                HttpSession session2 = request.getSession(true);
                lista = servicio.verDataByUsuario((String) session2.getAttribute("USERNAME"), (String) session2.getAttribute("PASSWORD"));
                String salida = "";
                if (lista != null) {

                    for (int cont = 0; cont < lista.size(); cont++) {
                        String lat = ""+lista.get(cont).getLat();
                        String lon = ""+lista.get(cont).getLon();
                        if (!lat.equalsIgnoreCase("0.0") && !lon.equalsIgnoreCase("0.0")) {
                            salida += "new google.maps.LatLng(" + lat + "," + lon + "),\n";
                        }
                    }

                } else {
                    salida += "new google.maps.LatLng(0,0,0),\n";
                }
                salida = salida.substring(0, salida.length() - 1);
            %>
            <%=salida%>
                    //new google.maps.LatLng(37.782551, -122.445368),
                    //new google.maps.LatLng(37.751266, -122.403355)
                    ];
                    function initialize() {
                    var mapOptions = {
                    zoom: 13,
            <%
                String out2 = "";
                if (lista!=null) {
                    out2 = "center: new google.maps.LatLng(" + lista.get(lista.size() - 1).getLat() + ", " + lista.get(lista.size() - 1).getLon() + "),";
                }
            %>
            <%=out2%>
                    //center: new google.maps.LatLng(37.774546, -122.433523),
                    mapTypeId: google.maps.MapTypeId.SATELLITE
                    };
                            map = new google.maps.Map(document.getElementById('map-canvas'),
                                    mapOptions);
                            var pointArray = new google.maps.MVCArray(taxiData);
            <%int indice = 0;%>
                    for (var cont = 0; cont < taxiData.length; cont++){
                    var marker = new google.maps.Marker({
                    position: taxiData[cont],
                            map: map,
                            title:
            <%
                String out3 = "";
                if (lista!=null) {
                    out3 += "'" + lista.get(indice).getTemp() + " celcius, " + lista.get(indice).getHum() + "% de humedad, "+lista.get(indice).getMp()+" MP'";
                }
                indice++;
            %>
            <%=out3%>
                    });
                    }
            <%indice = 0;%>
                    heatmap = new google.maps.visualization.HeatmapLayer({
                    data: pointArray
                    });
                            heatmap.setMap(map);
                    }
            function toggleHeatmap() {
            heatmap.setMap(heatmap.getMap() ? null : map);
            }

            function changeGradient() {
            var gradient = [
                    'rgba(0, 255, 255, 0)',
                    'rgba(0, 255, 255, 1)',
                    'rgba(0, 191, 255, 1)',
                    'rgba(0, 127, 255, 1)',
                    'rgba(0, 63, 255, 1)',
                    'rgba(0, 0, 255, 1)',
                    'rgba(0, 0, 223, 1)',
                    'rgba(0, 0, 191, 1)',
                    'rgba(0, 0, 159, 1)',
                    'rgba(0, 0, 127, 1)',
                    'rgba(63, 0, 91, 1)',
                    'rgba(127, 0, 63, 1)',
                    'rgba(191, 0, 31, 1)',
                    'rgba(255, 0, 0, 1)'
            ]
                    heatmap.set('gradient', heatmap.get('gradient') ? null : gradient);
            }

            function changeRadius() {
            heatmap.set('radius', heatmap.get('radius') ? null : 20);
            }

            function changeOpacity() {
            heatmap.set('opacity', heatmap.get('opacity') ? null : 0.2);
            }

            google.maps.event.addDomListener(window, 'load', initialize);</script>
    </head>

    <body>
        <div id="panel">
            <button onclick="toggleHeatmap()">Toggle Heatmap</button>
            <button onclick="changeGradient()">Change gradient</button>
            <button onclick="changeRadius()">Change radius</button>
            <button onclick="changeOpacity()">Change opacity</button>
        </div>
        <%=(lista==null?"No hay registro del usuario":"")%>
        <div id="map-canvas"></div>
        
    </body>
</html>

