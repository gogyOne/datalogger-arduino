<%-- 
    Document   : menuPrincipal
    Created on : 25-jun-2014, 13:31:31
    Author     : Usuario
--%>

<%@page import="webservice.ServicioTodo"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="css/miestilo.css" type="text/css">
    </head>
    <body>
        <h1>Hello World!</h1>
        <%
            HttpSession session2 = request.getSession(true);
            String out1 = "";
            String username = request.getParameter("username");
            String pass = request.getParameter("pass");
            session2.setAttribute("USERNAME", username);
            session2.setAttribute("PASSWORD", pass);
            ServicioTodo service = new ServicioTodo();
            //si existe en base de datos
            if (service.validarCuenta(username, pass) == true) {
                String tipoDeUsuario = service.getTipoUsuario(username, pass);
                if (tipoDeUsuario.equalsIgnoreCase("Admin")) {
                    response.sendRedirect("menuAdmin.jsp");
                } else if(tipoDeUsuario.equalsIgnoreCase("user")){
                    response.sendRedirect("menuUser.jsp");
                }else if(tipoDeUsuario.equalsIgnoreCase("usuario")){
                    response.sendRedirect("menuUser.jsp");
                }else {
                    response.sendRedirect("login.html");
                }
            } else {
                response.sendRedirect("login.html");
            }
        %>

    </body>
</html>
