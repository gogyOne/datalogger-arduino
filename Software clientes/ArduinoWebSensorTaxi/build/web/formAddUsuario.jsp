<%-- 
    Document   : formAddUsuario
    Created on : 25-jun-2014, 17:18:07
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es-cl">
    <head>
        <meta http-equiv="content-type"
              content="text/html; charset=UTF-8">
        <title>Ingreso Usuario</title>
        <link rel="stylesheet" href="css/miestilo.css" type="text/css">
    </head>
    <body>
        <div style="text-align: center;">
            <center>
                <form method="post" action="addUsuario.jsp" class="elegant-aero">
                    <table style="text-align: center; width: 500px; height: auto;"
                           border="1" cellpadding="2" cellspacing="2">
                        <tbody>
                            
                            <tr>
                                <td>nombre de usuario</td>
                                <td style="width: auto;"><input name="username" type="email"
                                                                value="Ingrese username" required></td>
                            </tr>
                            <tr>
                                <td>contraseña</td>
                                <td style="width: auto;"><input name="password"
                                                                type="password" required></td>
                            </tr>
                            <tr>
                                <td>Tipo de Usuario</td>
                                <td style="width: auto;">
                                    <select name="tipoUsuario">
                                        <option>Admin</option>
                                        <option>User</option>
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <input name="addUsuario" value="Agregar"
                           type="submit" class="button"><br>
                    <div style="text-align: left;"></div>
                </form>
            </center>
        </div>
    </body>
</html>
