<%-- 
    Document   : menuAdmin
    Created on : 25-jun-2014, 16:40:51
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es-cl">
    <head>
        <meta content="text/html; charset=UTF-8"
              http-equiv="content-type">
        <title>Menu Administrador</title>
        <link rel="stylesheet" href="css/miestilo.css" type="text/css">
    </head>
    <body>
        <div style="text-align: center;">
            <form method="post" action="formAddUsuario.jsp" class="elegant-aero"><input
                    name="addUsuario" value="Agregar Usuario" type="submit" class="button"></form>
            <form method="post" action="formEliminarUsuario.jsp" class="elegant-aero"><input
                    name="eliminarUsuario" value="Borrar Usuario"
                    type="submit" class="button"></form>
            <form method="post" action="mostrarUsuario.jsp" class="elegant-aero"><input
                    name="mostrarUsuario" value="Mostrar Usuarios"
                    type="submit" class="button"></form>
        </div>
    </body>
</html>
