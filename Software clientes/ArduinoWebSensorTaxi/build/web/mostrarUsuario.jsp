<%-- 
    Document   : mostrarUsuario
    Created on : 25-jun-2014, 17:26:55
    Author     : Usuario
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="domain.Usuario"%>
<%@page import="domain.Usuario"%>
<%@page import="webservice.ServicioTodo"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es-cl">
    <head>
        <meta content="text/html; charset=UTF-8"
              http-equiv="content-type">
        <title>Detalle de Usuarios</title>
        <link rel="stylesheet" href="css/miestilo.css" type="text/css">
    </head>
    <body>
    <center>
        <table style="text-align: left; width: 475px;" border="1"
               cellpadding="2" cellspacing="2" class="elegant-aero">
            <tbody>
                <tr>
                    <td style="width: 76px;">ID Usuario</td>
                    <td style="width: 76px;">Nombre de usuario</td>
                    <td style="width: 80px;">Contraseña</td>
                    <td style="width: 129px;">Tipo de Usuario</td>
                </tr>
                <% ServicioTodo service=new ServicioTodo();
                    List<Usuario> list=new ArrayList<Usuario>();
                    list=service.getTodoUsuario();
                    String salida="";
                    for(int cont=0;cont<list.size();cont++){
                        salida+="<tr>";
                        salida+="<td>"+list.get(cont).getIdUsuario()+"</td>";
                        salida+="<td>"+list.get(cont).getUsername()+"</td>";
                        salida+="<td>"+list.get(cont).getPassword()+"</td>";
                        salida+="<td>"+list.get(cont).getTipoUsuario()+"</td>";
                        salida+="</tr>";
                    }
                %>
                <%=salida%>
            </tbody>
        </table>
    </center>
    <br>
</body>
</html>

