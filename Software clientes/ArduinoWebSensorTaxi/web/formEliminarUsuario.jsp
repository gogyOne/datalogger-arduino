<%-- 
    Document   : formEliminarUsuario
    Created on : 25-jun-2014, 17:24:58
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es-cl">
    <head>
        <meta content="text/html; charset=UTF-8"
              http-equiv="content-type">
        <title>Eliminar Usuario</title>
        <link rel="stylesheet" href="css/miestilo.css" type="text/css">
    </head>
    <body>
    <center>
        <form method="post" action="eliminarUsuario.jsp" class="elegant-aero">
            <table style="text-align: left; width: 278px;" border="1"
                   cellpadding="2" cellspacing="2">
                <tbody>
                    <tr>
                        <td style="width: 98px;">Nombre de Usuario</td>
                        <td style="width: 160px;"><input name="username" type="email"
                                                         value="Ingrese username" required></td>
                    </tr>
                </tbody>
            </table>
            <input name="eliminar" value="Eliminar" type="submit" class="button"><br>
        </form>
    </center>
</body>
</html>
