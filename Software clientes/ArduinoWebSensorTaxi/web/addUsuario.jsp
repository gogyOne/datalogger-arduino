<%-- 
    Document   : addUsuario
    Created on : 25-jun-2014, 17:40:33
    Author     : Usuario
--%>

<%@page import="domain.Usuario"%>
<%@page import="webservice.ServicioTodo"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% 
        String username=request.getParameter("username");
        String password=request.getParameter("password");
        String tipoUsuario=request.getParameter("tipoUsuario");
        ServicioTodo servicio=new ServicioTodo();
        String salida=servicio.addUsuario(new Usuario(username, password, tipoUsuario));
        %>
        <%=salida %>
    </body>
</html>
