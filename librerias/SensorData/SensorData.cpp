#include <SoftwareSerial.h>
#include "arduino.h"
#include <TinyGPS.h>
#include "DHT.h"
#include "SensorData.h"
//establece el pin 5 para DHT22
#define DHTPIN 5
//establece el tipo de sensor
#define DHTTYPE DHT22
//crear un objeto con el pin 5 y sensor DHT22
DHT dht(DHTPIN, DHTTYPE);
//nuevo objeto gps
TinyGPS gps;
//nuevo objeto serial para GPS
SoftwareSerial ss(3, 4);
//almacena la tiempo actual de la funcion micros
unsigned long previo=0;
//constructor de la clase SensorData
SensorData::SensorData(uint8_t pinDHT, int pinLed, int pinSensorDust){
	//establece el objeto DHT
	DHT dht(pinDHT, DHTTYPE);
	//establece el baudio para GPS
	ss.begin(9600);
	//establece el pin del led del MP
	_pinLed=pinLed;
	//establece el pin del sensor de polucion 
	_pinSensorDust=pinSensorDust;
	//establece el led como salida
	pinMode(_pinLed,OUTPUT);
	//establece el sensor de polucion como entrada
	pinMode(_pinSensorDust, INPUT);
}
//devuelve la medida de material particulado en microgramos por metrocubico
float SensorData::getMP(){
	unsigned long start=micros();
	float voMeasured=0.0;
	//time-out de 280 microsengundos
	if(start-previo>=280){
		previo=start;
		digitalWrite(_pinLed,LOW); // apaga el led
	}
	
	//delayMicroseconds(280);//demora de 280 microsegundos
	//time-out de 40 microsengundos
	if(start-previo>=40){
		previo=start;
		voMeasured = analogRead(_pinSensorDust); // lee el valor de polucion
	}
 
	//delayMicroseconds(40); //demora de 40 microsegundos
	//time-out de 9680 microsengundos
	if(start-previo>=9680){
		previo=start;
		digitalWrite(_pinLed,HIGH); // enciende el led
	}
	
	//delayMicroseconds(9680); //demora de 9680 microsegundos
 
	// 0 - 5.0V mapped to 0 - 1023 integer values
	// recover voltage (convierte el valor de polucion a voltaje)
	float calcVoltage = voMeasured * (5.0 / 1024);
	//convierte voltaje en medida de material particulado en microgramos por metrocubico
	float dustDensity = (0.17 * calcVoltage - 0.1)*1000;
	//devuelve la polucion
	return dustDensity;
}
//obtiene la humedad en porcentaje
float SensorData::getHumedad() {
  //lee la medida de humedad
  float h = dht.readHumidity();
  //si existe un error
  if (isnan(h)) {
	//devuelve 0 si existe algun error
    return 0.0;
  } else {
    return h;
  }
}
//obtiene la temperatura en grado celcius
float SensorData::getTemperaturaC() {
  //lee la temperatura en grados celcius
  float t = dht.readTemperature();
  //si existe un error
  if (isnan(t)) {
	//devuelve 0 si existe algun error
    return 0.0;
  } else {
    return t;
  }
}
//obtener localizacion
String SensorData::getLocation() {
  //declara e inicializa latitud y longitud
  float flat=0.0, flon=0.0;
  //declara variables para gps
  bool newData = false;
  unsigned long chars;
  unsigned short sentences, failed;
  //por un segundo se parsea los datos del GPS y reporta algunos valores clave
  for (unsigned long start = millis(); millis() - start < 1000;)
  {
    while (ss.available())
    {
      char c = ss.read();
      // Serial.write(c); // uncomment this line if you want to see the GPS data flowing
      if (gps.encode(c)) // si hay una sentencia valida
        newData = true;
    }
  }

  if (newData)
  {
    
    unsigned long age;
	//obtiene la posicion GPS
    gps.f_get_position(&flat, &flon, &age);
	//almacena la latitud y la longitud
    flat=(flat == TinyGPS::GPS_INVALID_F_ANGLE) ? 0.0 : flat;
    flon=(flon == TinyGPS::GPS_INVALID_F_ANGLE) ? 0.0 : flon;
    gps.stats(&chars, &sentences, &failed);
  }
  String total="";
  //convierte la latitud en una cadena con doble presicion
  total.concat(String(precision6(flat)));
  total.concat(",");
  total.concat(String(precision6(flon)));
  return total;
}
//dar a un numero flotante precision 6
String SensorData::precision6(float b) {
  String resultado = "";
  //obtiene parte entera
  int entera = (int)b;
  //obtiene parte decimal
  float decimal = b - entera;
  //parte decimal convertido a cadena
  String partDes = String(long(abs(decimal * 1000000)));
  resultado.concat(entera);
  resultado.concat(".");
  resultado.concat(partDes);
  return resultado;
}
