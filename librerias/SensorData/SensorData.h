#ifndef SensorData_h
#define SensorData_h
#include "arduino.h"

class SensorData{
	public:
		SensorData(uint8_t pinDHT, int pinLed, int pinSensorDust);
		String precision6(float b);
		String getLocation();
		float getTemperaturaC();
		float getHumedad();
		float getMP();
	private:
		int _pinLed;
		int _pinSensorDust;
};
#endif