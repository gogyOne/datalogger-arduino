/*
Libejemplo.cpp -Descripción cpp
Creada por Nombre Autor, Fecha
Lanzado bajo licencia---
*/

#include "arduino.h"
#include "SDLib.h"
#include <SdFat.h>

//constructor de la clase SDLib
SDLib::SDLib(){
	Serial.println(".");
}
//almacena un dato en tarjeta SD solo si no existe previamente  
void SDLib::guardarData(char dato[]) {
  //si no existe el dato en la tarjeta SD, lo almacena
  if (isExiste(dato) == false) {
	//almacena el dato en la SD
	guardar(String(dato));
  }
}

/*	reescribe el dato existe para que tenga un 1 al principio. Devuelve verdadero si se reescribio,
	si no, devuelve falso. Es decir busca una linea que tenga 0, y reescribe con un 1 al principio
*/
boolean SDLib::rewrite(char datoViejo[]){
  boolean res=false;
  //declara la variable donde se almacena la linea
  char line[55];
  //se declara una variable que almacena posicion en el archivo en la tarjeta SD
  uint32_t pos;
  // abre un archivo de texto
  SdFile rdfile("FGETS.TXT", O_RDWR);
  // check for open error
  if (!rdfile.isOpen()){};
  //establece la posicion actual del archivo a cero
  rdfile.rewind();
  //lee las lineas desde el archivo para obtener la posicion
  while (1) {
    pos = rdfile.curPosition();
    if (rdfile.fgets(line, sizeof(line)) < 0) {
      res=false;
    }
    //encuentra la linea que contiene datoviejo
    if (strstr(line, datoViejo)){
      res=true;
      break;
    }
  }
  //modifica el primer valor de la variable
  datoViejo[0]='1';
  // rescribe la linea
  if (!rdfile.seekSet(pos)){};
  rdfile.println(datoViejo);
  //establece la posicion actual a 0
  rdfile.rewind();
  // una vez aplicados los cambios, cierra el archivo
  rdfile.close();
  return res;
}

/*
String SDLib::leerTodo(){
	String todo="";
	SdFile myFile;
	myFile.open("FGETS.TXT", O_READ);
	char data;
	while ((data = myFile.read()) >= 0) todo.concat(String(data));
	// close the file:
	myFile.close();
	return todo;
}
*/
/*almacena un dato en la memoria SD. Devuelve verdadero se guardó correctamente, 
si no, entonces devuelve falso.
*/
boolean SDLib::guardar(String dato){
	SdFile myFile;
	//si tiene problemas a abrir el archivo txt
	if (!myFile.open("FGETS.TXT", O_RDWR | O_CREAT | O_AT_END)) {
		return false;
	}else{
		//imprime en un archivo txt
		myFile.println(dato);
		//cierra el archivo txt
		myFile.close();
		return true;
	}
}

//devuelve verdadero si existe el dato en la memoria SD, si no, devuelve falso
boolean SDLib::isExiste(char dato[]){
  boolean res=false;
  //declara el buffer que almacena la linea
  const int line_buffer_size = 55;
  char buffer[line_buffer_size];
  //abre el archivo TGETS.TXT
  ifstream sdin("FGETS.TXT");
  //recorre el archivo txt en busca del dato
  while (sdin.getline(buffer, line_buffer_size, '\n') || sdin.gcount()) {
    int count = sdin.gcount();
    if (sdin.fail()) {
      sdin.clear(sdin.rdstate() & ~ios_base::failbit);
    } else if (sdin.eof()) {
      
    } else {
      count--;
    }
    if(strstr(buffer,dato)){
		res=true;
		break;
	}
  }
  
  return res;
}

//retorna una cadena que tenga el flag cero al principio, si no, entonces retorna null
String SDLib::getData0(){
  const int line_buffer_size = 55;
  char buffer[line_buffer_size];
  ifstream sdin("FGETS.TXT");
  String out="\0";
  while (sdin.getline(buffer, line_buffer_size, '\n') || sdin.gcount()) {
    int count = sdin.gcount();
    if (sdin.fail()) {
      sdin.clear(sdin.rdstate() & ~ios_base::failbit);
    } else if (sdin.eof()) {
   
    } else {
      count--;  // Don’t include newline in count
    }
	//si encuentra un 0 al principio, entonces parar la busqueda
    if(buffer[0]=='0'){
      out=String(buffer);
      break;
    }
  }
  return out;
}