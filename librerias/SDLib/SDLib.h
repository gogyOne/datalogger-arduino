/*
  Libejemplo.h - Descripción de la librería
Creada por Nombre Autor, Fecha
Lanzado bajo licencia --- 
*/
#ifndef SDLib_h
#define SDLib_h
#include "arduino.h"

class SDLib{
  public:
   SDLib();
   boolean rewrite(char datoViejo[]);
   boolean guardar(String dato);
   boolean isExiste(char dato[]);
   String getData0();
   void guardarData(char dato[]);
};
#endif
