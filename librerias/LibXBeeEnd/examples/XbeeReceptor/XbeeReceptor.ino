#include <LibXBeeEnd.h> //libreria encargada de recibir datos enviados por un modulo XBee
#include <XBee.h>
/*creamos un objeto, y establecemos el baudio configurado previamente en el modulo
*/
LibXBeeEnd xbee3(9600);

void setup() {
  Serial.begin(9600);
}

void loop() {
  /* 
  almacena el dato enviado por otro modulo XBee. Recibe "cualquier cosa".
  Devuelve nulo si no recibio nada. Si el actual XBee, recibio el dato, la funcion
  recibirPaquete() devuelve el dato recibido a secas como String. (explicacion "a secas")
  */
  String cadena = xbee3.recibirPaquete();
  
  if (cadena != "\0"&&cadena!="") {
    Serial.println(cadena);
  }
}
//y para rematar...
