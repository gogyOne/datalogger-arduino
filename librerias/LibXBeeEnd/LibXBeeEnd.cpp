#include "Arduino.h"
#include "LibXBeeEnd.h"
#include <XBee.h>
//crear objeto XBee
XBee xbee = XBee();
//crear objeto XBeeResponse
XBeeResponse response = XBeeResponse();
// create reusable response objects for responses we expect to handle
ZBRxResponse rx = ZBRxResponse();
ModemStatusResponse msr = ModemStatusResponse();
//contructor que establece los baudios
LibXBeeEnd::LibXBeeEnd(int baudio){
	Serial.begin(baudio);
	xbee.setSerial(Serial);
}
//devuelve el paquete de datos enviado desde sistema emisor
String LibXBeeEnd::recibirPaquete(){
	//prepara variable de retorno como nulo
	String salida="";
	//lee el paquete de datos
	xbee.readPacket();
	if (xbee.getResponse().isAvailable()) {
    // ha obtenido algo
    
    if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE) {
      // a obtenido un paquete zb rx

      // llena la clase zb rx
      xbee.getResponse().getZBRxResponse(rx);
      if (rx.getOption() == ZB_PACKET_ACKNOWLEDGED) {
		//asigna el paquete recibido a la variable dataString
		char dataString[rx.getDataLength()];
        for (int cont = 0; cont < sizeof(dataString); cont++) {
          dataString[cont] = byte(rx.getData(cont));
        }
		//contruye una cadena con el dato recibido desde xbee coordinator, para despues retornar
		for(int cont=0;cont<sizeof(dataString);cont++){
			salida+=dataString[cont];
		}
      }
    } else if (xbee.getResponse().getApiId() == MODEM_STATUS_RESPONSE) {
      xbee.getResponse().getModemStatusResponse(msr);
      // the local XBee sends this response on certain events, like association/dissociation

      if (msr.getStatus() == ASSOCIATED) {
        
      } else if (msr.getStatus() == DISASSOCIATED) {
        
      } else {
        
      }
    } 
  }
  //devulve el paquete de datos recibidos, si no, entonces devuelve vacio
  return salida;
}