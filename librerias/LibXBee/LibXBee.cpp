#include "arduino.h"
#include "LibXBee.h"
#include <XBee.h>
// create the XBee object
XBee xbee2 = XBee();

// establece la direccion SH + SL del XBee receptor
XBeeAddress64 addr64 = XBeeAddress64(0x0013A200, 0x40A1EBC4);
ZBTxRequest zbTx;
ZBTxStatusResponse txStatus = ZBTxStatusResponse();
//constructor de la clase LibXBee
LibXBee::LibXBee(int baudio, uint32_t sh, uint32_t sl){
	//establce los baudios
	Serial.begin(baudio);
	//establece el SH y SL
	addr64 = XBeeAddress64(sh, sl);
	//establece los baudios en XBee
	xbee2.setSerial(Serial);
}
/*enviar paquete de datos via protocolo XBee. Devuelve verdadero si el paquete fue recibido, si no,
entonces devuelve falso*/
boolean LibXBee::enviar(String dato){
	boolean res=false;
	//establece el tamaño del paquete a enviar
	uint8_t payload[dato.length()];
    //almacena paquete en una variable compatible con XBee (payload)
	for (int cont = 0; cont <dato.length(); cont++) {
      payload[cont] = dato.charAt(cont);
    }
	
	//prepara el paquete a enviar
    zbTx = ZBTxRequest(addr64, payload, sizeof(payload));
    //envia el paquete de datos
	xbee2.send(zbTx);
	/* despues de enviar la solictud tx, esperamos una respuesta de estado.
	se espera medio segundo para la respuesta de estado
	*/
    if (xbee2.readPacket(500)) {
      // a obtenido una respuesta
	  
	  // deberia ser un estado znet tx
      if (xbee2.getResponse().getApiId() == ZB_TX_STATUS_RESPONSE) {
        xbee2.getResponse().getZBTxStatusResponse(txStatus);
		// obtiene el estado de entrega
        if (txStatus.getDeliveryStatus() == SUCCESS) {
          // a recibido el paquete exitosamente
          res=true;
        }
      }
    }
	return res;
}