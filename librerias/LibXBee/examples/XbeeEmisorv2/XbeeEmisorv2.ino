
#include <LibXBee.h>//libreria encargada de enviar datos via XBee
#include <XBee.h>
/*crea un objeto, en el cual, establece los baudios,SH y SL
*/
LibXBee xbee3(9600, 0x0013A200, 0x407A26DB);
//almacena la demora al enviar un dato de un segundo
int delayData = 1000;
unsigned long previousMillis = 0;
//establece los pines de los botones
int botonUp = 2;
int botonLeft = 3;
int botonRight = 4;

void setup() {
  Serial.begin(9600);
}
void loop() {
  unsigned long start = millis();
  //delay pero con millis determinado por la variable delayData
  if (start - previousMillis >= delayData) {
    previousMillis = start;
    String string="nada";
    //lee el valor de los botones
    int vUp = digitalRead(botonUp);
    int vRight = digitalRead(botonRight);
    int vLeft = digitalRead(botonLeft);
    /*si hay una pulsacion del boton, entonces asigna
      al boton, un nombre.
    */
    if (vUp == 0) {
      string = "up";
    } else if (vRight == 0) {
      string = "right";
    } else if (vLeft == 0) {
      string = "left";
    } else {
      string = "nada";
    }
    /*envia un string al xbee remoto. 
      La funcion devuelve true si la cadena llegó, si no llegó,
      entonces devuelve falso
    */
    xbee3.enviar(string);
  }

}
